import { FETCH_MODELS, VOTE_MODEL } from './../actions/types';
const INITIAL_STATE = [];

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case FETCH_MODELS:
			return [...action.payload];

		case VOTE_MODEL:
			return state.map(model => model.id === action.payload.id ? action.payload : model);

		default:
			return state;
	}
}