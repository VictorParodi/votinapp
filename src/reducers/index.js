import { combineReducers } from 'redux';
import modelsReducer from './modelsReducer';

export default combineReducers({
	models: modelsReducer
});