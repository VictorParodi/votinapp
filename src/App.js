import React from 'react';
import { BrowserRouter, Switch, Redirect } from 'react-router-dom';
import Header from './components/Header';
import Footer from './components/Footer';
import { ROUTES } from './routes';
import './App.css';

function App() {
	return (
		<main>
			<BrowserRouter>
				<Header />
				<Switch>
					{ROUTES}
					<Redirect to='/' />
				</Switch>
				<Footer />
			</BrowserRouter>
		</main>
	);
}

export default App;
