import React from 'react';
import MainLabel from './../../ui-library/MainLabel';

function PastTrials() {
	return (
		<section>
			<article>
				<MainLabel content="Past Trials Page" />
			</article>
		</section>
	);
}

export default PastTrials;