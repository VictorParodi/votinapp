import React from 'react';
import MainLabel from './../../ui-library/MainLabel';

function AuthPage() {
	return (
		<section>
			<article>
				<MainLabel content="Authentication Page" />
			</article>
		</section>
	);
}

export default AuthPage;