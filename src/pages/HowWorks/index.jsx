import React from 'react';
import MainLabel from './../../ui-library/MainLabel';

function HowWorks() {
	return (
		<section>
			<article>
				<MainLabel content="How It Works Page" />
			</article>
		</section>
	);
}

export default HowWorks;