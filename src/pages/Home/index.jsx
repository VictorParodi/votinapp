import React from 'react';
import CardList from './../../components/CardList';
import BasicButton from './../../ui-library/BasicButton';
import './Home.css';

function Home() {
	return (
		<section>
			<article className="top-banner">
				<div className="top-banner-content">
					<p className="left-text">Speak out. Be heard. <span>Be counted</span></p>
					<p className="middle-text">
						Lorem ipsum dolor sit amet consectetur, adipisicing elit. Voluptatum repellat eius,
						unde iste provident beatae in ipsum exercitationem itaque minima ad? Tempora provident totam quasi odit? Laborum cum illo voluptate!
					</p>
					<i className="fa fa-times fa-2x close-icon"></i>
				</div>
			</article>

			<CardList />

			<article className="bottom-banner">
				<div className="bottom-banner-bgr"></div>
				<div className="bottom-banner-content">
					<p className="text-bottom-banner">Is there anyone else you would want us to add?</p>
					<BasicButton
						className="bottom-banner-btn"
						content="Submit a Name"
						type="dark"
					/>
				</div>
				<div className="bottom-banner-dimmer"></div>
			</article>
		</section>
	);
}

export default Home;