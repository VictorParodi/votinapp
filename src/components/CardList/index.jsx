import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { fetchModels } from './../../actions';
import Card from './../Card';
import './CardList.css';

function CardList() {
	const models = useSelector(state => state.models);
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(fetchModels());
	}, [dispatch]);

	const renderCards = (cards) => {
		return cards.map(cardItem => {
			return <Card key={cardItem.id} cardData={cardItem} />
		});
	}

	return (
		<article className="card-list">
			<h1 className="cardlist-title">Votes</h1>

			<div className="cards-wrapper">
				{
					models
						? renderCards(models)
						: null
				}
			</div>
		</article>
	);
}

export default CardList;