const getPencentage = (data) => {
	const thumbUpAmount = data.thumbUp;
	const thumbDownAmount = data.thumbDown;
	const thumbTotal = thumbUpAmount + thumbDownAmount;
	const upPercent = Math.round((thumbUpAmount * 100) / thumbTotal);
	const downPercent = Math.round((thumbDownAmount * 100) / thumbTotal);
	const upLabel = `${upPercent}%`;
	const downLabel = `${downPercent}%`;

	return { up: upLabel, down: downLabel };
}

export { getPencentage };