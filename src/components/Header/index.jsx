import React from 'react';
import { Link } from 'react-router-dom';
import HeaderCard from './../HeaderCard';
import { CONFIG } from './../../routes';
import './Header.css';

function Header() {
	const renderMenuItems = () => {
		return CONFIG.map(({ id, label, path }) => {
			return (
				<li key={id} className="menu-item">
					<Link to={path} className="menu-link">
						{label}
					</Link>
				</li>
			);
		});
	}

	return (
		<header className="header-wrapper">
			<div className="header-content">
				<div className="menu-container">
					<span className="header-label">LionApp</span>

					<ul className="menu">
						{renderMenuItems()}
						<li className="menu-item">
							<i className="fa fa-search fa-2x"></i>
						</li>
					</ul>

					<i className="fa fa-bars mobile-menu-icon"></i>
				</div>

				<HeaderCard />
			</div>
		</header>
	);
}

export default Header;