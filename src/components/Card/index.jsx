import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { voteModel } from './../../actions';
import Thumb from './../../ui-library/Thumb';
import BasicButton from './../../ui-library/BasicButton';
import { getPencentage } from './../helpers/bars';
import './Card.css';

function Card({ cardData }) {
	const dispatch = useDispatch();
	const [voteType, setVoteType] = useState('');
	const [buttonText, setButtonText] = useState({ text: 'Vote now', active: true });
	const card = cardData || {};
	const id = card.id || null;
	const name = card.name || '';
	const style = card.picture ? { backgroundImage: `url(${card.picture})` } : { backgroundImage: '' };
	const percentLabels = getPencentage({ thumbUp: cardData.thumbUp, thumbDown: cardData.thumbDown });

	const onVoting = () => {
		if (voteType) {
			const voteObj = { [voteType]: cardData[voteType] + 1 };
			dispatch(voteModel(id, voteObj));
			alert('Thanks for voting !!!');
			setButtonText({ text: 'Vote again', active: false });
			setVoteType('');
		} else {
			setButtonText({ text: 'Vote now', active: true });
		}
	}

	const voting = (text) => {
		setVoteType(text);
	}



	return (
		<div className="cards" style={style}>
			<div className="cards-title">
				<div className="label">{name}</div>
				<div className="metainfo">1 month ago <span>in Disney</span></div>
			</div>

			<div className="cards-content">
				<span>Thanks so much for your vote!!!</span>
			</div>

			<div className="cards-actions">
				{
					buttonText.active
						? (
							<div>
								<Thumb
									type="up"
									clickable={true}
									vote={voting}
									active={voteType === 'thumbUp' ? true : false}
								/>
								<Thumb
									className="thumb-down"
									type="down"
									clickable={true}
									vote={voting}
									active={voteType === 'thumbDown' ? true : false}
								/>
							</div>
						)
						: null
				}

				<BasicButton
					className="vote-button"
					content={buttonText.text}
					type='light'
					vote={onVoting}
				/>
			</div>

			<div className="cards-bars">
				<div className="bar-up" style={{ width: `${percentLabels.up}` }}>
					<i className="fa fa-thumbs-up fa-2x"></i>
					<span>{percentLabels.up}</span>
				</div>

				<div className="bar-down" style={{ width: `${percentLabels.down}` }}>
					<i className="fa fa-thumbs-down fa-2x"></i>
					<span>{percentLabels.down}</span>
				</div>
			</div>

			<div className="cards-dimmer"></div>
		</div>
	);
}

export default Card;