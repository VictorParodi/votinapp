import React from 'react';
import './Footer.css';
const ITEMS = [
	{
		id: 1,
		text: 'Terms and Conditions',
	},
	{
		id: 2,
		text: 'Privacy Policy',
	},
	{
		id: 3,
		text: 'Contact Us',
	}
];

function Footer() {
	const renderItems = () => {
		return ITEMS.map(({ id, text }) => <li key={id} className="footer-item">{text}</li>);
	}

	return (
		<article className="footer-wrapper">
			<div className="footer-content">
				<ul className="footer-menu">
					{renderItems()}
				</ul>

				<ul className="footer-icons">
					<li className="footer-icon">Follow Us</li>
					<li className="footer-icon">
						<i className="fa fa-facebook-square fa-3x"></i>
					</li>
					<li className="footer-icon">
						<i className="fa fa-twitter fa-3x"></i>
					</li>
				</ul>
			</div>
		</article>
	);
}

export default Footer;