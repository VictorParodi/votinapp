import React from 'react';
import './HeaderCard.css';

function HeaderCard() {
	return (
		<article className="card-wrapper">
			<div className="card-dimmer"></div>

			<div className="card">
				<div className="card-title">
					<div className="meta">Lorem ipsum dolor sit amet consectetur</div>
					<div className="title">Main Card Text</div>
				</div>

				<div className="card-content">
					<div className="main-content">
						Lorem ipsum dolor sit amet consectetur adipisicing elit. Quibusdam consequuntur dolor ut sunt, vero ipsam,
						facilis excepturi consequatur ab ea quia nesciunt atque aut quam? Magnam, odit. Quam, magnam error.
					</div>

					<div className="meta-content">
						<a href="#" className="meta-link">Meta content</a>
						<span className="meta-text">What's Your Veredict</span>
					</div>
				</div>

				<div className="card-actions">
					<div className="action up">
						<i className="fa fa-thumbs-up fa-3x"></i>
					</div>

					<div className="action down">
						<i className="fa fa-thumbs-down fa-3x"></i>
					</div>
				</div>
			</div>
		</article>
	);
}

export default HeaderCard;