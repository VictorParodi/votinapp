import React from 'react';
import './BasicButton.css';

function BasicButton({ className, content, type, vote }) {
	const compType = type ? type : 'light';
	const compClass = className ? className : null;
	const compContent = content ? content : null;
	const buttonClass = `basic-button-component ${compClass}`;

	const lightStyles = {
		border: '1px solid #FFF',
		color: '#FFF'
	};

	const darkStyles = {
		border: '1px solid #000',
		color: '#000'
	};

	const isDark = compType === 'dark';
	const compStyle = isDark ? darkStyles : lightStyles;

	const voting = () => {
		vote();
	}

	return (
		compContent
			? (
				<button className={buttonClass} style={compStyle} onClick={voting}>
					{ compContent}
				</button>
			)
			: compContent
	);
}

export default BasicButton;