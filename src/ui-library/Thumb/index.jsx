import React from 'react';
import './Thumb.css';

function Thumb({ className, type, clickable, vote, active }) {

	const compType = type ? type : 'up';
	const compClass = className ? className : null;
	let thumbClass = `thumb-component ${compClass}`;
	thumbClass = active ? `${thumbClass} active` : thumbClass;
	// const isClickable = clickable === true ? clickable : false;

	const upStyles = {
		backgroundColor: 'rgba(44, 187, 180, 1)',
		color: '#FFF'
	};

	const downStyles = {
		backgroundColor: 'rgba(255, 181, 51, 1)',
		color: '#FFF'
	};

	const isUp = compType === 'up';
	const compStyle = isUp ? upStyles : downStyles;
	const compContent = isUp ? <i className="fa fa-thumbs-up fa-2x"></i> : <i className="fa fa-thumbs-down fa-2x"></i>

	const votingUp = () => {
		vote('thumbUp');
	}

	const votingDown = () => {
		vote('thumbDown');
	}

	const voting = isUp ? votingUp : votingDown;

	return (
		<button className={thumbClass} style={compStyle} onClick={voting}>
			{compContent}
		</button>
	);
}

export default Thumb;