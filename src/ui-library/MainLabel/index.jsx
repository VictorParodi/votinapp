import React from 'react';
import './MainLabel.css';

function MainLabel({ content }) {
	const text = (content && typeof content === 'string') ? content : 'Main Label Text';

	return (
		<div className="main-label">
			<h1 className="main-label-text">{text}</h1>
		</div>
	);
}

export default MainLabel;