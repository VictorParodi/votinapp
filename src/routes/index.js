import React from 'react';
import { Route } from 'react-router-dom';
import Home from './../pages/Home';
import PastTrials from './../pages/PastTrials';
import HowWorks from './../pages/HowWorks';
import AuthPage from './../pages/AuthPage';

const CONFIG = [
	{
		id: 1,
		label: 'Home',
		path: '/',
		component: Home
	},

	{
		id: 2,
		label: 'Past Trials',
		path: '/past-trials',
		component: PastTrials
	},

	{
		id: 3,
		label: 'How It Works',
		path: '/how-works',
		component: HowWorks
	},

	{
		id: 4,
		label: 'Log In / Sign Up',
		path: '/auth',
		component: AuthPage
	}
];

const ROUTES = CONFIG.map(({ id, path, component }) => <Route key={id} path={path} component={component} exact />);

export { CONFIG, ROUTES };