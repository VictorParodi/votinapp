import models from './../apis/models';
import { FETCH_MODELS, VOTE_MODEL } from './types';

export const fetchModels = () => {
	return async (dispatch) => {
		const response = await models.get('/models');
		dispatch({
			type: FETCH_MODELS,
			payload: response.data
		});
	}
}

export const voteModel = (id, vote) => {
	return async (dispatch) => {
		const response = await models.patch(`/models/${id}`, vote);
		dispatch({
			type: VOTE_MODEL,
			payload: response.data
		});
	}
}