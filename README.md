This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### Services

This project uses json-server as a server data provider. In order to have the data available follow these steps:
1. Clone the repo from here https://bitbucket.org/VictorParodi/votinapp-server/src/master
2. Install the json-server package in you machine: npm install -g json-server
3. Go to the folder where the repo was cloned and run the server data provider using this command: json-server --watch db.json --port 3001

If you want to see the data in the borswer open [http://localhost:3001/models]

Finally, run the react aplication using the proper command.
